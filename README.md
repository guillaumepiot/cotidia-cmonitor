# Install

	pip install git+https://guillaumepiot@bitbucket.org/guillaumepiot/cotidia-cmonitor.git

# Settings

Add the API key to your settings:

	CMONITOR_API_KEY = 'Your CM API key'

Add to installed apps:

	INSTALLED_APPS = (
	    ...
	    'cmonitor',
	)

Add your list(s):

	CMONITOR_LISTS = (
		('<list_id>', '<list name>'),
		...
	)

If there's one list only, the subscription form will show the email only.

If there's more than one list, then the subscription for will display a list of list to subscribe to.

# Database

CMonitor saves the subsription locally as well for back up purpose, so you will need to sync your database:

	python manage.py syncdb

# URLs

Include this line to your URLs patterns:

	# CMonitor
    url(r'^subscribe/', include('cmonitor.urls')),

# Template

You will require `jquery.form.js` to submit the form via AJAX.

	{% load cmonitor_tags %}

	{# The HTML form #}
	{% subsribe_form %}

	{# The JS to handle the form #}
	{% subsribe_form_js %}