import os
from distutils.core import setup
from setuptools import find_packages


VERSION = __import__("cmonitor").VERSION

CLASSIFIERS = [
    'Framework :: Django',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: BSD License',
    'Operating System :: OS Independent',
    'Topic :: Software Development',
]

install_requires = [
    'createsend',
]

# taken from django-registration
# Compile the list of packages available, because distutils doesn't have
# an easy way to do this.
packages, data_files = [], []
root_dir = os.path.dirname(__file__)
if root_dir:
    os.chdir(root_dir)

for dirpath, dirnames, filenames in os.walk('cmonitor'):
    # Ignore dirnames that start with '.'
    for i, dirname in enumerate(dirnames):
        if dirname.startswith('.'): del dirnames[i]
    if '__init__.py' in filenames:
        pkg = dirpath.replace(os.path.sep, '.')
        if os.path.altsep:
            pkg = pkg.replace(os.path.altsep, '.')
        packages.append(pkg)
    elif filenames:
        prefix = dirpath[9:] # Strip "cmonitor/" or "cmonitor\"
        for f in filenames:
            data_files.append(os.path.join(prefix, f))


setup(
    name="cotidia-cmonitor",
    description="Plug-in for Campaign Monitor subscription form",
    version=VERSION,
    author="Guillaume Piot",
    author_email="guillaume@cotidia.com",
    url="https://bitbucket.org/guillaumepiot/cotidia-cmonitor",
    download_url="https://bitbucket.org/guillaumepiot/cotidia-cmonitor/downloads/cotidia-cmonitor-%s.tar.gz" % VERSION,
    package_dir={'cmonitor': 'cmonitor'},
    packages=packages,
    package_data={'cmonitor': data_files},
    include_package_data=True,
    install_requires=install_requires,
    classifiers=CLASSIFIERS,
)
