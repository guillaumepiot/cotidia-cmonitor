from django.db import models
from django.forms import ModelForm

class Subscriber(models.Model):
	email = models.EmailField('Email')
	name = models.EmailField('Name', blank=True)
	date = models.DateTimeField(auto_now=True)
	lists = models.TextField('Lists')
	
	def __unicode__(self):
		return u'%s' % (self.email)	