from django.template import Library, Node, TemplateSyntaxError, VariableDoesNotExist, resolve_variable
from django.conf import settings

from cmonitor.forms import SubscribeForm
register = Library()



@register.inclusion_tag('cmonitor/form.html', takes_context=True)
def subsribe_form(context):
	request = context['request']
	initial = {}
	if request:
		if request.GET.get('email', False):
			initial = {'email':request.GET['email']}
	form = SubscribeForm(initial=initial)
	return { 'form' : form }
	
@register.inclusion_tag('cmonitor/form.js')
def subsribe_form_js():

	return {}
