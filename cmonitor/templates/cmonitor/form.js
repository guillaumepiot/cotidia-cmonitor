{% load i18n %}
// Requires the jquery.form.js package

var formdiv = '#cm_form'
var options = { 
    beforeSubmit: function(){
        $(formdiv).find('input[type=submit]').attr('value','{% trans "Sending..." %}');
    },
    success:    function(data) { 
    	console.log(data);
    	$(formdiv).find('input[type=submit]').attr('value','{% trans "Subscribe" %}');
        // Remove prior error messages
        $(formdiv+' .alert').remove();
        if(data.error){
            $.each(data.error, function(index, value) { 
            	console.log(formdiv+' [name="'+index+'"]');
               // Add relevant error messages
                $(formdiv+' [name="'+index+'"]:first').before('<div class="alert alert-danger">'+value+'</div>');
            })
        }
        if(data.success){
            $(formdiv).html('<div class="alert alert-success">{% trans "Thank you for your subscription." %}</div>');
        }
        
    } 
}; 

$(formdiv).ajaxForm(options);
