from createsend import CreateSend, BadRequest
from createsend import Subscriber as CMSubscriber
from django.conf import settings
from cmonitor.models import Subscriber
from cmonitor.forms import SubscribeForm
from django.http import HttpResponse

if not settings.CMONITOR_API_KEY:
	raise Exception('Please specify CMONITOR_API_KEY in settings.py')


def process(request):
	import json 
	form = SubscribeForm(data=request.POST if request.method == 'POST' else False)
	success = False
	error = False
	if request.method == 'POST':
		show = True
		
		success= False
		#print request.POST
		if form.is_valid():

			try:

				subscriber = CMSubscriber({'api_key': settings.CMONITOR_API_KEY})
				lists = form.cleaned_data['lists']
				if type(lists) is list:
					for list_id in lists:
						id = subscriber.add(list_id, form.cleaned_data['email'],"",[],True)
				else:
					id = subscriber.add(lists, form.cleaned_data['email'],"",[],True)

				f = Subscriber.objects.create(email=form.cleaned_data['email'], lists=form.cleaned_data['lists'])
				f.save()

				success= 'success'
			except BadRequest as br:
			  error = "Bad request error: %s" % br
			  error += "Error Code:	 %s" % br.data.Code
			  error +=  "Error Message: %s" % br.data.Message
			except Exception as e:
			  error = "Error: %s" % e
			
			
			
		else:
			error = form.errors

		
				 
	results = {'success':success, 'error':error}
	return HttpResponse(
        json.dumps(results),
        content_type = 'application/json'
    )