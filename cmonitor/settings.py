from django.conf import settings

# The API key
CMONITOR_API_KEY = getattr(settings, 'CMONITOR_API_KEY', False)

# The client ID
CMONITOR_CLIENT_ID = getattr(settings, 'CMONITOR_CLIENT_ID', False)

# The lists a user can subscribe to, if one only, then it will be subscribed to that one by default
CMONITOR_LISTS = getattr(settings, 'CMONITOR_LISTS', False)