from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from cmonitor.models import Subscriber

if not settings.CMONITOR_LISTS:
	raise Exception('Please specify a tuple of subscription lists in settings.py with CMONITOR_LISTS')

class SubscribeForm(forms.ModelForm):
	email = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
	lists = forms.MultipleChoiceField(label=_('Choose your subscription'), choices=settings.CMONITOR_LISTS, widget=forms.CheckboxSelectMultiple())
	
	class Meta:
		model = Subscriber
		fields=('email', 'lists')

	def __init__(self, *args, **kwargs):
		super(SubscribeForm, self).__init__(*args, **kwargs)
		print len(settings.CMONITOR_LISTS)
		if len(settings.CMONITOR_LISTS)  == 1:
			self.fields['lists'] = forms.CharField(initial=settings.CMONITOR_LISTS[0][0], widget=forms.HiddenInput)